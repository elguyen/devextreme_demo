import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import DE_ServerSide from './views/DE_ServerSide.vue'
import masterview from './views/masterGridView/demo.vue'
import lineChart from '@/components/LineChart.vue'

Vue.use(Router)

export default new Router({
  routes: [{
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/devextreme/serverside',
      name: 'DE_ServerSide',
      component: DE_ServerSide
    },
    {
      path: '/devextreme/masterView',
      name: 'masterView',
      component: masterview
    }, {
      path: '/devextreme/linechart',
      name: 'lineChart',
      component: lineChart
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import( /* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})